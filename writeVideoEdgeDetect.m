function writeVideoEdgeDetect

% create writer object for prewitt method
writerObj = VideoWriter('Prewitt-Comb-video-by-VideoWriter.avi');
writerObj.FrameRate = 14;
open(writerObj);

% create writer object for canny method
writerObj1 = VideoWriter('Canny-Comb-video-by-VideoWriter.avi');
writerObj1.FrameRate = 14;
open(writerObj1);

% read video 1 and set initial values
obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov');
get(obj)

% read video 2 and set initial values
obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
get(obj1)
nFrames1 = obj1.NumberOfFrames;
vidHeight1 = obj1.Height;
vidWidth1 = obj1.Width;

% resize and combine frames
for k=1:nFrames1
    % read the kth frame from video 1
    temp = read(obj,k);
    % resize this frame to the same size as the one in video 2
    after_resize = imresize(temp, [vidHeight1 vidWidth1]);
    % read the kth frame from video 2
    temp = read(obj1,k);
    % combine the kth frame from both videos
    comb_frame = [after_resize temp];
    % convert image to grayscale since edge method request 2D
    I = rgb2gray(comb_frame);
    % apply edge detection with Prewitt and Canny method
    BW1 = edge(I,'prewitt');
    BW2 = edge(I,'canny');
    % write the combined frame into video object, to be notice, you have to
    % convert the image to double so that it can be written into video
   
    writeVideo(writerObj,im2double(BW1));   
    writeVideo(writerObj1,im2double(BW2));
end

end
