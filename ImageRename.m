function ImageRename

Subject = '/Users/yanchen/Desktop/visualfingerprint/DataForFaceDete/Testing';

InputFolder = dir(Subject);
Name_Space = {'Andrew' 'Brennan' 'Charles' 'Daniel' 'Dorothy' 'Dylan'...
    'Emilia' 'Emily' 'Eric' 'Erin' 'Grace' 'Hyun' 'Ian' 'Kaia' 'LuAnn'...
    'Michelle' 'Nathan' 'NathanS' 'Nicholas' 'Richard' 'Ryan' 'Sam'...
    'Sean' 'Seth' 'Tessa' 'Tommy' 'William' };
    
for i = 1:size(InputFolder,1)
      Convention=[];
      k=0;
    if( not(strcmp(InputFolder(i).name,'.')...
            |strcmp(InputFolder(i).name,'Thumbs.db')...
            |strcmp(InputFolder(i).name,'.DS_Store')...
            |strcmp(InputFolder(i).name,'..')))
        
        % Change the subject name to subject ID
        Name_Split = strsplit(InputFolder(i).name,'.');
        
        for j=1:27
            if strcmp(Name_Split{1},Name_Space{j})==1
                Convention{1} = num2str(j);
                k=1;
                break           
            end
        end
        
        if k==0
            continue
        end
        
        Convention{2} = 'task';
        
        % figure out the tasks number.
        if str2num(Name_Split{2}) < 6
            Convention{3}='1';
        elseif (str2num(Name_Split{2}) > 5) & (str2num(Name_Split{2}) < 11)
            Convention{3}='5';
        else
            Convention{3}='2';
        end
        
        Convention{4} = Name_Split{3};
        
        % check if it is detected
        if strcmp(Convention{4},'detected')==0
            Convention{5} = Name_Split{2};
            Convention{6} = Name_Split{4};
        elseif strcmp(Convention{4},'detected')==1 & length(Name_Split)==4
            if InputFolder(i).bytes>1000
                Convention{5} = '1';
                Convention{6} = Name_Split{2};
                Convention{7} = Name_Split{4};
            else
                Convention{5} = '0';
                Convention{6} = Name_Split{2};
                Convention{7} = Name_Split{4};
            end
        else
            Convention{5} = Name_Split{4};
            Convention{6} = Name_Split{2};
            Convention{7} = Name_Split{5};
        end
        
        Convention
        Old_Name = strcat(Subject,strcat('/',InputFolder(i).name));
        New_Name = strcat('/',strjoin(Convention,'.'));
        New_Name = strcat(Subject,New_Name);
        
        movefile(Old_Name,New_Name);
        
        
    end
end