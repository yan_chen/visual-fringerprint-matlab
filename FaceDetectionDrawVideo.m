function FaceDetectionDrawVideo


faceDetector = vision.CascadeObjectDetector();

% frame = imread('/Users/yanchen/Desktop/Screen Shot 2013-09-30 at 3.28.13 PM.png');

% create writer object.
writerObj = VideoWriter('/Users/yanchen/Desktop/visualfingerprint/UMD_Data/DrawRama.avi')
writerObj.FrameRate = 14;
open(writerObj);

% read video 1 and set initial values
obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/UMD_Data/rama1.mov');
get(obj)

% read video 2 and set initial values
% obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
% get(obj1)
nFrames = obj.NumberOfFrames;


csvwrite('data.csv',zeros(1,5));
% resize and combine frames
for k=1:nFrames
    % read the kth frame from video 1
    temp = read(obj,k);
    
    bbox = step(faceDetector, temp);
    videoOut = insertObjectAnnotation(temp,'rectangle',bbox,'Face');   
    
%     if isempty(bbox)
%         M = [k zeros(1,4)];
%         dlmwrite('data.csv',M,'-append');
%     else
%         bbox = sortrows(bbox,[3 4]);
%         M = [k bbox(size(bbox,1),:)];
%         dlmwrite('data.csv',M,'-append');
%     end
   
        
   % write the combined frame into video object
    writeVideo(writerObj,videoOut);
end

end
