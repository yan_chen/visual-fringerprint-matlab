function FaceDetectionVideo


faceDetector = vision.CascadeObjectDetector();
videoFile = vision.VideoFileReader('/Users/yanchen/Desktop/visualfingerprint/matlab/Comb-video-by-VideoWriter.avi');
videoFrame      = step(videoFile);
% frame = imread('/Users/yanchen/Desktop/Screen Shot 2013-09-30 at 3.28.13 PM.png');

% create writer object.
writerObj = VideoWriter('/Users/yanchen/Desktop/visualfingerprint/data/FaceDetectionVideo.avi')
writerObj.FrameRate = 14;
open(writerObj);

% read video 1 and set initial values
obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov');
get(obj)

% read video 2 and set initial values
obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
get(obj1)
nFrames1 = obj1.NumberOfFrames;
vidHeight1 = obj1.Height;
vidWidth1 = obj1.Width;

csvwrite('data.csv',zeros(1,5));
% resize and combine frames
for k=1:nFrames1
    % read the kth frame from video 1
    temp = read(obj,k);
    % resize this frame to the same size as the one in video 2
    after_resize = imresize(temp, [vidHeight1 vidWidth1]);
    % read the kth frame from video 2
    temp = read(obj1,k);
    % combine the kth frame from both videos
    comb_frame = [after_resize temp];
    
    bbox = step(faceDetector, comb_frame);
  %  videoOut = insertObjectAnnotation(comb_frame,'rectangle',bbox,'Face');
    
    
    if isempty(bbox)
        M = [k zeros(1,4)];
        dlmwrite('data.csv',M,'-append');
    else
        bbox = sortrows(bbox,[3 4]);
        M = [k bbox(size(bbox,1),:)];
        dlmwrite('data.csv',M,'-append');
    end
   
        
    % write the combined frame into video object
   % writeVideo(writerObj,videoOut);
end

end
