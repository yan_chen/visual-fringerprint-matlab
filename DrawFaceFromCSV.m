function DrawFaceFromCSV

bbox = csvread('/Users/yanchen/Desktop/visualfingerprint/data/SensorDrawonFrame.csv');

videoFile = vision.VideoFileReader('/Users/yanchen/Desktop/visualfingerprint/data/DrawFromCSV.avi');

% writerObj = VideoWriter('/Users/yanchen/Desktop/visualfingerprint/data/DrawSensorData.avi')
% writerObj.FrameRate = 14;
% open(writerObj);
% i=2;

videoFrame = step(videoFile);

I = videoFrame;
text_str = cell(6,1);

i=1;

text_str{1} = ['ACCELEROMETER X(m/s^2): ' num2str(bbox(i,1))];
text_str{2} = ['ACCELEROMETER Y(m/s^2): ' num2str(bbox(i,2))];
text_str{3} = ['ACCELEROMETER Z(m/s^2): ' num2str(bbox(i,3))];

text_str{4} = ['GYROSCOPE X(rad/s): ' num2str(bbox(i,4))];
text_str{5} = ['GYROSCOPE Y(rad/s): ' num2str(bbox(i,5))];
text_str{6} = ['GYROSCOPE Z(rad/s): ' num2str(bbox(i,6))];

%text_str{6} = ['Time(YYYY-MO-DD HH-MI-SS_SSS: ' num2str(bbox(i,7))];

position = [10 10; 10 40; 10 70; 10 100; 10 130; 10 160];


RGB = insertText(I, position, text_str, 'FontSize', 18, 'BoxOpacity', 0.4);


figure, imshow(RGB), title('Board');

% while ~isDone(videoFile)
%     videoFrame = step(videoFile);
%     videoFrame = [videoFrame 
%     M = [bbox(i,2) bbox(i,3) bbox(i,4) bbox(i,5)];
%     videoOut = insertObjectAnnotation(videoFrame,'rectangle',M,'Face');
%     
%     writeVideo(writerObj,videoOut);
%     i=i+1;
%     
% end

end
