function FaceDetectionImageForRestImage

Dir_Path = '/Users/yanchen/Desktop/visualfingerprint/DataForFaceDete/RestData';
Dir = dir(Dir_Path);
Detect = '/Users/yanchen/Desktop/visualfingerprint/DataForFaceDete/RestData/Detected';
mkdir(Detect);

faceDetector = vision.CascadeObjectDetector('MergeThreshold',6);
Detected_Face_Image = [0 0; 155 208];
for j = 1:size(Dir,1)
    % loop for each subject folder
    if ( not(strcmp(Dir(j).name,'.')...
            |strcmp(Dir(j).name,'Thumbs.db')...
            |strcmp(Dir(j).name,'.DS_Store')...
            |strcmp(Dir(j).name,'..')))
        
        % name of each folder path
        Subject = strcat(Dir_Path,'/',Dir(j).name);
        InputFolder = dir(Subject);
        
        for i = 1:size(InputFolder,1)
            
            if( not(strcmp(InputFolder(i).name,'.')...
            |strcmp(InputFolder(i).name,'Thumbs.db')...
            |strcmp(InputFolder(i).name,'.DS_Store')...
            |strcmp(InputFolder(i).name,'..')))
                % split the file name by detecting dot
                Name_Split = strsplit(InputFolder(i).name,'.');

                % only detecting the original images
                if (length(Name_Split{2}) == 4) & (length(Name_Split{4}) == 8)
                   % Find image path
                   File_Name = strcat('/',InputFolder(i).name);
                   File_Path = strcat(Subject,File_Name);
                   Original_Image = imread(File_Path);

                   % store detected face position into a 1x4 matrix
                   Detected_Face_Location = step(faceDetector, Original_Image)

                   if (~isempty(Detected_Face_Location) & max(Detected_Face_Location(:,3))>100)
                       % crop the detected face from the original image
                       Detected_Face_Location = sortrows(Detected_Face_Location,-3);
                       Detected_Face_Image = imcrop(Original_Image,Detected_Face_Location(1,:));
                   
                       
                   end

                   % save detected face image as the right name
                   % find subject name by splitting the file name

                   % change the third string to 'detected'
                   Name_Split{4} = 'detected';
                   Name_Split{7} = Name_Split{6};
                   Name_Split{6} = Name_Split{5};
                   if size(Detected_Face_Image,1)<10
                        Name_Split{5} = '0';
                   else
                        Name_Split{5} = '1';
                   end
                  
                   Detect_Face_Name = strjoin(Name_Split,'.');
                   Detect_Face_Name = strcat('/',Detect_Face_Name);
                   Detect_Face_Name = strcat(Detect,Detect_Face_Name)
                   imwrite(Detected_Face_Image,Detect_Face_Name);
                   
                   Detected_Face_Image = [0 0; 155 208];
                end
            end
        end
    end
end
        




%%%%%%%%%%%%%%%%%%%
% Subject = '/Users/yanchen/Desktop/visualfingerprint/DataForFaceDete/KaiaSargent';
% Detect = '/Users/yanchen/Desktop/visualfingerprint/DataForFaceDete/Detected';
% mkdir(Detect);
% InputFolder = dir(Subject);
% faceDetector = vision.CascadeObjectDetector('MergeThreshold',6);
% 
% Detected_Face_Image = [0 0; 155 208];
% for i = 1:size(InputFolder,1)
%       
%     if( not(strcmp(InputFolder(i).name,'.')...
%             |strcmp(InputFolder(i).name,'Thumbs.db')...
%             |strcmp(InputFolder(i).name,'.DS_Store')...
%             |strcmp(InputFolder(i).name,'..')))
%        
%         % split the file name by detecting dot
%         Name_Split = strsplit(InputFolder(i).name,'.');
%       
%         % only detecting the original images
%         if length(Name_Split{3})~=7
%            % Find image path
%            File_Name = strcat('/',InputFolder(i).name);
%            File_Path = strcat(Subject,File_Name);
%            Original_Image = imread(File_Path);
%            
%            % store detected face position into a 1x4 matrix
%            Detected_Face_Location = step(faceDetector, Original_Image)
% 
%            if (~isempty(Detected_Face_Location) & max(Detected_Face_Location(:,3))>100)
%                % crop the detected face from the original image
%                Detected_Face_Location = sortrows(Detected_Face_Location,-3);
%                Detected_Face_Image = imcrop(Original_Image,Detected_Face_Location(1,:));
%            end
%            
%            % save detected face image as the right name
%            % find subject name by splitting the file name
%           
%            % change the third string to 'detected'
%            Name_Split(3) = cellstr('detected');
%            Detect_Face_Name = strjoin(Name_Split,'.')
%            Detect_Face_Name = strcat('/',Detect_Face_Name);
%            Detect_Face_Name = strcat(Detect,Detect_Face_Name)
%            imwrite(Detected_Face_Image,Detect_Face_Name);
%            Detected_Face_Image = [0 0; 155 208];
%         end
%     end
% end

