function writeVideo

% create writer object.
writerObj = VideoWriter('Comb-video-by-VideoWriter.avi')
writerObj.FrameRate = 14;
open(writerObj);

% read video 1 and set initial values
obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov');
get(obj)

% read video 2 and set initial values
obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
get(obj1)
nFrames1 = obj1.NumberOfFrames;
vidHeight1 = obj1.Height;
vidWidth1 = obj1.Width;

% resize and combine frames
for k=1:nFrames1
    % read the kth frame from video 1
    temp = read(obj,k);
    % resize this frame to the same size as the one in video 2
    after_resize = imresize(temp, [vidHeight1 vidWidth1]);
    % read the kth frame from video 2
    temp = read(obj1,k);
    % combine the kth frame from both videos
    comb_frame = [after_resize temp];
    % write the combined frame into video object
    writeVideo(writerObj,comb_frame);
end

end
