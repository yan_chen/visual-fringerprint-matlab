function FaceDetectionThroughVideo

faceDetector = vision.CascadeObjectDetector('MergeThreshold',4);

% folder read directory
Subject = '/Users/yanchen/Desktop/visualfingerprint/FaceVideo/TaskVideo';
Subject_Path = dir(Subject);
% folder save directory
Save_Path_Name = '/Users/yanchen/Desktop/visualfingerprint/FaceVideo/TaskFace';

Task_Space_1 = {'3','4','7','11','13','17','18','20','21','23','26','27'};
Task_Space_2 = {'2','9','19'};
Task_Space_3 = {'12','24'};



for i=1:size(Subject_Path,1)
     if not(strcmp(Subject_Path(i).name,'.')|strcmp(Subject_Path(i).name,'..')...
            |strcmp(Subject_Path(i).name,'Thumbs.db')|strcmp(Subject_Path(i).name,'.DS_Store'))
        
        % index of training image
        k = 1;
        
        % detect subject ID
        Subject_Infor = strsplit(Subject_Path(i).name,'.');
        Subject_ID = Subject_Infor{1};
       
        if ~isempty(find(ismember(Task_Space_1,Subject_ID)))
            TaskSpace = 1;
        elseif ~isempty(find(ismember(Task_Space_2,Subject_ID)))
            TaskSpace = 2;
        elseif ~isempty(find(ismember(Task_Space_3,Subject_ID)))
            TaskSpace = 3;
        elseif ~strcmp('14',Subject_ID)
            TaskSpace = 4;
        else
            TaskSpace = 5;
        end
        
        
        % create a foler to save the original images and detected faces
        path = strcat('/',Subject_ID);
        Save_Path = strcat(Save_Path_Name,path);
        mkdir(Save_Path);
        
%         % create a csv file to save the hight and width of the face
%         Subject_CSV_Path = strcat(Subject,'/','data.csv');
%         csvwrite(Subject_CSV_Path,zeros(1,3));
%         M = [0 0 0]; % default data
        
        % video file path
        Subject_Video_Path = strcat(Subject,'/',Subject_Path(i).name);
        
        % read vieo
        videoFile = VideoReader(Subject_Video_Path);
        
        % number of frames
        NF = videoFile.NumberOfFrames
        
        for j = 1:12:NF
            % read jth frame in the video
            temp = read(videoFile,j);
            Detected_Face_Image = [0 0; 2 2];
            % store detected face position into a 1x4 matrix
            Detected_Face_Location = step(faceDetector, temp)
           
           if (~isempty(Detected_Face_Location) & max(Detected_Face_Location(:,3))>100)
               % crop the detected face from the original image
               % sort the face location array so that the largest size will
               % be the first element in the array
               Detected_Face_Location = sortrows(Detected_Face_Location,-3);
               Detected_Face_Image = imcrop(temp,Detected_Face_Location(1,:));
%                M = [1 Detected_Face_Location(1,3) Detected_Face_Location(1,4)];
%                dlmwrite(Subject_CSV_Path,M,'-append');
               Detected = '1';
               
           else
               Detected = '0';
           end
           
           % task index
           if TaskSpace == 1
               if k<601
                   TaskInd = '1';
               elseif (k>600) & (k<1201)
                   TaskInd = '5';
               elseif (k>1200) & (k<1801)
                   TaskInd = '6'
               elseif (k>1800) & (k<2401)
                   TaskInd = '3';
               elseif (k>2400) & (k<3001)
                   TaskInd = '2';
               else
                   break
               end
           end
           
           if TaskSpace == 2
               if k<601
                   TaskInd = '1';
               elseif (k>600) & (k<1201)
                   TaskInd = '2';
               elseif (k>1200) & (k<1801)
                   TaskInd = '3'
               elseif (k>1800) & (k<2401)
                   TaskInd = '4';
               else
                   break
               end
           end
           
           if TaskSpace == 3
               if k<601
                   TaskInd = '1';
               elseif (k>600) & (k<1201)
                   TaskInd = '5';
               elseif (k>1200) & (k<1801)
                   TaskInd = '2'
               elseif (k>1800) & (k<2401)
                   TaskInd = '3';
               elseif (k>2400) & (k<3001)
                   TaskInd = '4';
               else
                   break
               end
           end
        
           if TaskSpace == 4
               if k<601
                   TaskInd = '1';
               elseif (k>600) & (k<1201)
                   TaskInd = '5';
               elseif (k>1200) & (k<1801)
                   TaskInd = '6'
               elseif (k>1800) & (k<2401)
                   TaskInd = '3';
             
               else
                   break
               end
           end
           
           if TaskSpace == 5
               if k<601
                   TaskInd = '1';
               elseif (k>600) & (k<1201)
                   TaskInd = '5';
               elseif (k>1200) & (k<1801)
                   TaskInd = '6'
               elseif (k>1800) & (k<2401)
                   TaskInd = '2';
               elseif (k>2400) & (k<3001)
                   TaskInd = '3';
               else
                   break
               end
           end


           % name space
%            Training_Original_Name = strcat(Save_Path,'/',Subject_ID,'.','task.',TaskInd,'.original.',num2str(k),'.jpg');
           Training_Detected_Name = strcat(Save_Path,'/',Subject_ID,'.','task.',TaskInd,'.detected.',Detected,'.',num2str(k),'.jpg');
           % save detected faces
           imwrite(Detected_Face_Image,Training_Detected_Name);
           % save original images
%            imwrite(temp,Training_Original_Name);
           k = k+1;

           end
          
        end
     end
end
% 
% faceDetector = vision.CascadeObjectDetector();
% videoFile = vision.VideoFileReader('/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/Calibration.mov');
% videoFrame      = step(videoFile);
% % frame = imread('/Users/yanchen/Desktop/Screen Shot 2013-09-30 at 3.28.13 PM.png');
% Save = dir('/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/CaliFace');
% % create writer object.
% writerObj = VideoWriter('/Users/yanchen/Desktop/visualfingerprint/data/FaceDetectionVideo.avi')
% writerObj.FrameRate = 14;
% open(writerObj);
% 
% % read video 1 and set initial values
% obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov');
% get(obj)
% 
% % read video 2 and set initial values
% obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
% get(obj1)
% nFrames1 = obj1.NumberOfFrames;
% vidHeight1 = obj1.Height;
% vidWidth1 = obj1.Width;
% 
% csvwrite('data.csv',zeros(1,5));
% % resize and combine frames
% for k=1:nFrames1
%     % read the kth frame from video 1
%     temp = read(obj,k);
%     % resize this frame to the same size as the one in video 2
%     after_resize = imresize(temp, [vidHeight1 vidWidth1]);
%     % read the kth frame from video 2
%     temp = read(obj1,k);
%     % combine the kth frame from both videos
%     comb_frame = [after_resize temp];
%     
%     bbox = step(faceDetector, comb_frame);
%   %  videoOut = insertObjectAnnotation(comb_frame,'rectangle',bbox,'Face');
%     
%     
%     if isempty(bbox)
%         M = [k zeros(1,4)];
%         dlmwrite('data.csv',M,'-append');
%     else
%         bbox = sortrows(bbox,[3 4]);
%         M = [k bbox(size(bbox,1),:)];
%         dlmwrite('data.csv',M,'-append');
%     end
%    
%         
%     % write the combined frame into video object
%    % writeVideo(writerObj,videoOut);
% end
% 
% end
