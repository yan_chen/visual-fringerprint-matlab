function FaceDetectionImageRename

Subject = '/Users/yanchen/Desktop/visualfingerprint/FaceVideo/TaskFace/1';

InputFolder = dir(Subject);

for i = 1:size(InputFolder,1)
      
    if( not(strcmp(InputFolder(i).name,'.')...
            |strcmp(InputFolder(i).name,'Thumbs.db')...
            |strcmp(InputFolder(i).name,'.DS_Store')...
            |strcmp(InputFolder(i).name,'..')))
       
       
           
        % split the file name by detecting dot
        Name_Split = strsplit(InputFolder(i).name,'.');
        
        if strcmp(Name_Split{3},'detected') == 1
            
                    k = str2num(Name_Split{5})

                       if k<601
                           TaskInd = '1';
                       elseif (k>600) & (k<1201)
                           TaskInd = '5';
                       elseif (k>1200) & (k<1801)
                           TaskInd = '6'
                       elseif (k>1800) & (k<2401)
                           TaskInd = '3';
                       elseif (k>2400) & (k<3001)
                           TaskInd = '2';
                       else
                           TaskInd = '4';
                       end
                TaskInd

                Name_Split{7}=Name_Split{6};
                Name_Split{6}=Name_Split{5};
                Name_Split{5}=Name_Split{4};
                Name_Split{4}=Name_Split{3};
                Name_Split{3}=num2str(TaskInd);


                Old_Name = strcat(Subject,strcat('/',InputFolder(i).name));
                New_Name = strcat('/',strjoin(Name_Split,'.'));
                New_Name = strcat(Subject,New_Name);

                movefile(Old_Name,New_Name);
        
         end
    end
end

