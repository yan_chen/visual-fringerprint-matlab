function FaceDetectionCaliVideo

faceDetector = vision.CascadeObjectDetector('MergeThreshold',6);

% folder read directory
Subject = '/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/TestVideo';
Subject_Path = dir(Subject);
% folder save directory
Save_Path_Name = '/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/CaliFace';


for i=1:size(Subject_Path,1)
     if not(strcmp(Subject_Path(i).name,'.')|strcmp(Subject_Path(i).name,'..')...
            |strcmp(Subject_Path(i).name,'Thumbs.db')|strcmp(Subject_Path(i).name,'.DS_Store'))
        
        % index of training image
        k = 1;
        
        % detect subject ID
        Subject_Infor = strsplit(Subject_Path(i).name,'.');
        Subject_ID = Subject_Infor{1};
       
        % create a foler to save the original images and detected faces
        path = strcat('/',Subject_ID);
        Save_Path = strcat(Save_Path_Name,path);
        mkdir(Save_Path);
        
        % video file path
        Subject_Video_Path = strcat(Subject,'/',Subject_Path(i).name);
        
        % read vieo
        videoFile = VideoReader(Subject_Video_Path);
        
        % number of frames
        NF = videoFile.NumberOfFrames;
        
        for j = 1:12:NF
            % read jth frame in the video
            temp = read(videoFile,j);
            
            % store detected face position into a 1x4 matrix
            Detected_Face_Location = step(faceDetector, temp);

           if (~isempty(Detected_Face_Location) & max(Detected_Face_Location(:,3))>100)
               % crop the detected face from the original image
               % sort the face location array so that the largest size will
               % be the first element in the array
               Detected_Face_Location = sortrows(Detected_Face_Location,-3);
               Detected_Face_Image = imcrop(temp,Detected_Face_Location(1,:));
               
               % name space
               Training_Original_Name = strcat(Save_Path,'/',Subject_ID,'.','training.original.',num2str(k),'.jpg');
               Training_Detected_Name = strcat(Save_Path,'/',Subject_ID,'.','training.detected.',num2str(k),'.jpg');
               % save detected faces
               imwrite(Detected_Face_Image,Training_Detected_Name);
               % save original images
               imwrite(temp,Training_Original_Name);
               k=k+1;
               
               if k==121, break, end                   
           end
          
        end
     end
end
% 
% faceDetector = vision.CascadeObjectDetector();
% videoFile = vision.VideoFileReader('/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/Calibration.mov');
% videoFrame      = step(videoFile);
% % frame = imread('/Users/yanchen/Desktop/Screen Shot 2013-09-30 at 3.28.13 PM.png');
% Save = dir('/Users/yanchen/Desktop/visualfingerprint/NicholasSullivan10-16-13/CaliFace');
% % create writer object.
% writerObj = VideoWriter('/Users/yanchen/Desktop/visualfingerprint/data/FaceDetectionVideo.avi')
% writerObj.FrameRate = 14;
% open(writerObj);
% 
% % read video 1 and set initial values
% obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov');
% get(obj)
% 
% % read video 2 and set initial values
% obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov');
% get(obj1)
% nFrames1 = obj1.NumberOfFrames;
% vidHeight1 = obj1.Height;
% vidWidth1 = obj1.Width;
% 
% csvwrite('data.csv',zeros(1,5));
% % resize and combine frames
% for k=1:nFrames1
%     % read the kth frame from video 1
%     temp = read(obj,k);
%     % resize this frame to the same size as the one in video 2
%     after_resize = imresize(temp, [vidHeight1 vidWidth1]);
%     % read the kth frame from video 2
%     temp = read(obj1,k);
%     % combine the kth frame from both videos
%     comb_frame = [after_resize temp];
%     
%     bbox = step(faceDetector, comb_frame);
%   %  videoOut = insertObjectAnnotation(comb_frame,'rectangle',bbox,'Face');
%     
%     
%     if isempty(bbox)
%         M = [k zeros(1,4)];
%         dlmwrite('data.csv',M,'-append');
%     else
%         bbox = sortrows(bbox,[3 4]);
%         M = [k bbox(size(bbox,1),:)];
%         dlmwrite('data.csv',M,'-append');
%     end
%    
%         
%     % write the combined frame into video object
%    % writeVideo(writerObj,videoOut);
% end
% 
% end
