function [] = Combo()

% read video 1 and set initial values
obj = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/FaceTest.mov','Tag','My reader obj');
get(obj)
nFrames = obj.NumberOfFrames;
vidHeight = obj.Height;
vidWidth = obj.Width;
mov(1:nFrames) = struct('cdata',zeros(vidHeight,vidWidth,3,'uint8'),'colormap',[]);
for k=1:nFrames
mov(k).cdata = read(obj,k);
end

hf = figure;

set(hf,'position',[100 100 600 400])
movie(hf,mov,1,obj.FrameRate);
hf = figure;
set(hf,'position',[100 100 vidHeight vidWidth])

% play movie
movie(hf,mov,1,obj.FrameRate);

% imshow(mov(1).cdata)
% O = mov(1).cdata;
% movie is consist of frames, and each frame can be treated as a matrx
% image
size(O)


% read video 2 and set initial values
obj1 = VideoReader('/Users/yanchen/Desktop/visualfingerprint/data/DisplayTest.mov','Tag','My reader obj');
get(obj1)
nFrames1 = obj1.NumberOfFrames;
vidHeight1 = obj1.Height;
vidWidth1 = obj1.Width;
mov1(1:nFrames1) = struct('cdata',zeros(vidHeight1,vidWidth1,3,'uint8'),'colormap',[]);
for k=1:nFrames1
    mov1(k).cdata = read(obj1,k);
end
 

hf1 = figure(2)

set(hf1,'position',[100 100 vidWidth1 vidHeight1])
movie(hf1,mov1,1,obj1.FrameRate);

% resize one of the videos so that they can be combined later
mov3(1:nFrames1) = struct('cdata',zeros(vidHeight1,vidWidth1,3,'uint8'),'colormap',[]);
for k=1:nFrames1
    mov3(k).cdata = imresize(mov(k).cdata, [vidHeight1 vidWidth1]);
end

% check the size size(mov3(1).cdata)

% combine two videos
mov4(1:nFrames1) = struct('cdata',zeros(vidHeight1,vidWidth1,3,'uint8'),'colormap',[]);
for k=1:nFrames1
    mov4(k).cdata = [mov3(k).cdata mov1(k).cdata];
end


hf2 = figure(2);
set(hf2,'position',[100 100 800 1100])

% play the final videos
movie(hf2,mov4,1,obj1.FrameRate);

end

